<aside class="main-sidebar sidebar-dark-primary elevation-4 ">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="{{  asset('image/siteSettings/' . siteTitle()[1]) }}" alt="Site_Logo"
            class="brand-image img-circle" style="opacity: .9">
        <span class="brand-text font-weight-light">{!! siteTitle()[0] !!}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('image/admin/' . profileImage() ) }}" class="img-circle"
                    alt="User Image">
            </div>
            
        {{-- {!! dd(siteTitle()) !!} --}}
            <div class="info">
                <a href="#" class="d-block">{{ username() }}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                <li class="nav-item menu-open">
                    <a href="" class="nav-link ">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('index') }}" class="nav-link {!! \Request::route()->getName() == 'index' ? 'active' : '' !!}">
                                <i class="fa fa-home nav-icon"></i>
                                <p>Home</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('category.index') }}" class="nav-link {!! in_array(\Request::route()->getName(),CategoryRouts()) ? 'active':'' !!}">
                                <i class="far fa-list-alt nav-icon"></i>
                                <p>Category</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{!! route('product-index') !!}" class="nav-link {!! in_array(\Request::route()->getName(),ProductRouts()) ? 'active':'' !!}">
                                <i class="fa fa-shopping-basket nav-icon "></i>
                                <p>Product</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{!! route('site-settings.index') !!}" class="nav-link {!! \Request::route()->getName() == 'site-settings.index' ? 'active' : '' !!}" >
                                <i class="fa fa-cog nav-icon "></i>
                                <p>Site-Settings</p>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</aside>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        @if (Session::has('fail'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ Session::get('fail') }}
            </div>
        @endif
        @if (Session::has('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ Session::get('success') }}
            </div>
        @endif
    </div>
    @yield('main')
    <div class="clearfix hidden-md-up"></div>
</div>
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
