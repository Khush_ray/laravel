@extends('layout.app')
@section('title', 'Category')
@section('head', 'Categories')
@section('main')
    <section class="content">
        <div class="container-fluid">
            <!-- Default box -->
            <div class="card card-outline card-info collapsed-card">
                <div class="card-header">
                    <h3 class="card-title">Search</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-plus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body" style="display: none;">
                    <form id="search_form" method="POST">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" name="categoy_search" placeholder="Category Name" id="categoy_search"
                                        class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select class="form-control " name="category_status" id="category_status">
                                        <option value="" selected>Status</option>
                                        <option value="active">Active</option>
                                        <option value="inactive">Deactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="submit" value="Search" name="search" class="btn btn-info">
                                    <input type="button" value="Reset" name="reset" class="btn btn-danger" id="clear_btn">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-12 col-lg-6">
                            <h3 class="font-weight-bold">{!! $title !!}</h3>
                        </div>

                        <div class="col-sm-12 col-lg-6">
                            <a href="{!! route('category.categoryAdd') !!}" type="button"
                                class=" btn btn-primary float-right"><i class="fa fa-plus"
                                    aria-hidden="true">&nbsp;&nbsp;</i> Add New Record!</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-borderless table-hover dtTable" id="categoryTable">
                        <thead>
                            <tr>
                                <th data-data="id" data-name="id">ID</th>
                                <th data-data="v_image" data-name="v_image" data-orderable=false>Image</th>
                                <th data-data="v_name" data-name="v_name">Name</th>
                                <th data-data="dt_added_on" data-name="dt_added_on" >Added Date</th>
                                <th data-data="dt_modified_on" data-name="dt_modified_on" >Modified Date/Time</th>
                                <th data-data="no_product" data-name="no_product" >No. of Products</th>
                                <th data-data="i_order" data-name="i_order">Order</th>
                                <th data-data="ti_status" data-name="ti_status">Status</th>
                                <th data-data="action" data-name="action" data-orderable=false>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var catTable = applyDtTable('#categoryTable', "{{ route('category.loadList') }}");
            // console.log(catTable);
            $(document).on("click", ".delete", function(e) {
                e.preventDefault;
                var delete_id = $(this).data('id');
                Swal.fire({
                    title: 'Ssly Bruh! U wanna Delete it?',
                    imageUrl: "{{ asset('assets/theme/img/error.gif') }}",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        var data = {
                            "id": delete_id,
                        };
                        jQuery.ajax({
                            url: "{!! route('category.categoryDelete') !!}",
                            type: "POST",
                            data: data,
                            dataType: "json",
                            success: function(res) {

                                if (res.success) {
                                    Swal.fire(
                                        'Deleted!',
                                        'Your file has been deleted.',
                                        'success'
                                    )
                                    $('.dtTable').DataTable().ajax.reload();
                                }
                            }
                        });
                    }
                })
            });
        });

        function applyDtTable(id, url) {
            // console.log(id);
            return $(id).DataTable({
                searching: false,
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: url,
                    type: 'POST',
                    data: function(d) {
                        d.search = $('#categoy_search').val();
                        d.status = $("#category_status").val();
                    }
                }
            })
        }
        $('#search_form').on('submit', function(e) {
            e.preventDefault();
            $('.dtTable').DataTable().ajax.reload();
        });

        $("#clear_btn").click(function(e) {
            e.preventDefault();
            $("#categoy_search").val('');
            $("#category_status").val('');
            $('.dtTable').DataTable().ajax.reload();
        });
    </script>
@endpush
