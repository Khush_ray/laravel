<?php
function siteTitle()
{
   $data = [];
   $siteTile = \App\Models\SiteSettings::all();
   foreach ($siteTile as $key => $val) {
      $data[] = $val->l_val;
   }

   return $data;
}
function username()
{
   return  Illuminate\Support\Facades\Auth::user() ?  Illuminate\Support\Str::ucfirst(Illuminate\Support\Facades\Auth::user()->email) : '';
}

function profileImage()
{
   return Illuminate\Support\Facades\Auth::user() ? Illuminate\Support\Facades\Auth::user()->v_img : '';
}

function ProductRouts()
{

   $product = [
      'product-index',
      'product.product-list',
      'product.prodcut-add',
      'product.prodcut-edit'
   ];

   return $product;
}

function CategoryRouts()
{

   $category = [
      'category.index',
      'category.loadList',
      'category.categoryAdd',
      'category.categoryEdit'
   ];

   return $category;
}

function countCategories()
{
   $count = App\Models\Category::count();
   return $count ? $count : 0;
}
function countCategoriesActive()
{
   $count = App\Models\Category::where('ti_status', '=', '1')->count();
   return $count ? $count : 0;
}
function countCategoriesDeactive()
{
   $count = App\Models\Category::where('ti_status', '=', '0')->count();
   return $count ? $count : 0;
}


function countProducts()
{
   $count = App\Models\Product::count();
   return $count ? $count : 0;
}
function countProductsActive()
{
   $count = App\Models\Product::where('ti_status', '=', '1')->count();
   return $count ? $count : 0;
}
function countProductsDective()
{
   $count = App\Models\Product::where('ti_status', '=', '0')->count();
   return $count ? $count : 0;
}
function dateTime()
{
   $date = Carbon\Carbon::now();
   return $date;
}

function ImageInsert($imageData)
{
   $imageUpload = [];

   foreach ($imageData as $key => $file) {
      $validext = ['jpg', 'jpeg', 'png', 'gif'];
      $ext = $file->extension();
      if (in_array($ext, $validext)) {
         $name = time() . '_' . mt_rand(1, 100000) . '.' . $ext;
         $file->move(public_path('image/product'), $name);
         $imageUpload[] = $name;
         // dd($imageUpload);
      } else {
         $imageUpload = ['err' => "Image must be in jpg/jpeg/png/gif format"];
      }
   }
   return $imageUpload;
}
