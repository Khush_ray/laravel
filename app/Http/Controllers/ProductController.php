<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductCategories;
use App\Models\ProductImage;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use \Carbon\Carbon;

class ProductController extends Controller
{
    public function loadList()
    {
        $query  = Product::query();
        $query = $query->select('tbl_product.*', 'tbl_product_img.v_image', 'tbl_product_categories.bi_category_id', DB::raw('GROUP_CONCAT(DISTINCT `tbl_categories`.`v_name`) as `v_category_name`'));
        $query = $query->leftJoin('tbl_product_img', 'tbl_product.id', '=', 'tbl_product_img.i_product_id');
        $query = $query->leftJoin('tbl_product_categories', 'tbl_product.id', '=', 'tbl_product_categories.bi_product_id');
        $query = $query->leftJoin('tbl_categories', 'tbl_product_categories.bi_category_id', '=', 'tbl_categories.id');

        if (request('search')) {
            $query = $query->where('tbl_product.v_name', 'like', '%' . request('search') . '%');
        }
        if (request('priceMin')) {
            $query = $query->where('f_price', '>', request('priceMin'));
        }
        if (request('priceMax')) {
            $query = $query->where('f_price', '<', request('priceMax'));
        }
        if (request('qtyMin')) {
            $query = $query->where('i_qty', '>', request('qtyMin'));
        }
        if (request('qtyMax')) {
            $query = $query->where('i_qty', '<', request('qtyMax'));
        }
        if (!empty(request('status')) && request('status') == 'active') {
            $query = $query->where('tbl_product.ti_status', 1);
        } elseif (!empty(request('status')) && request('status') == 'inactive') {
            $query = $query->where('tbl_product.ti_status', 0);
        }
        $query = $query->where('tbl_product_img.ti_main_image', '=', 1);
        $query = $query->groupBy('tbl_product.id');
        $query = $query->orderBy(request('columns.' . request('order.0.column') . '.name'), request('order.0.dir'));

        $total = count($query->get());
        $list = $query->skip(request('start'))->limit(request('length'))->get();
        $data = [];
        foreach ($list as $key => $val) {
            $data[] = [
                'id' =>  $val->id,
                'v_image' => '<img height="100px" width="120px" src="' . asset('image/product/' . $val->v_image) . '">',
                'v_name' => $val->v_name,
                "i_product_code" => $val->i_product_code,
                "v_category_name" => $val->v_category_name,
                "f_price" => $val->f_price,
                "f_sale_price" => $val->f_sale_price,
                "i_qty" => $val->i_qty,
                'dt_added_on' => $val->dt_added_on,
                'dt_modified_on' =>  $val->dt_modified_on,
                'i_order' =>  $val->i_order,
                'ti_status' => ($val->ti_status) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>',
                'action' => '</div><a  class="btn btn-warning" href=' . route('product.prodcut-edit', $val->id) . '><i class="fas fa-pencil-alt"></i></a> <button  data-id="' . $val->id . '" class="btn btn-danger delete"><i class="fas fa-trash"></i></button>'
            ];
        }
        return response()->json([
            'draw'  => request('draw'),
            'recordsTotal' => $total,
            'recordsFiltered' => $total,
            'data' => $data
        ]);
    }

    public function index()
    {
        $title = 'Product-List';
        return view('product.list', ['title' => $title]);
    }

    public function form($id = 0)
    {
        $title = $id ? 'Edit Product' : 'Add Product';
        $product = Product::find($id);
        $product = $product ? $product : new Product;
        $urlsave = $id ? route('product.prodcut-save', [$product->id]) : route('product.prodcut-save', [0]);
        $productImg = ProductImage::where('i_product_id', '=', $id)->get();
        $categoryList = Category::all();
        $productCategories = ProductCategories::where('bi_product_id', '=', $id)->get();
        $selected = [];
        if (!empty($productCategories)) {
            foreach ($productCategories as $key => $value) {
                $selected[$key] = $value->bi_category_id;
            }
        }
        return view('product.form', ['product' => $product, 'urlSave' => $urlsave, 'title' => $title, 'image' => $productImg, 'categoryList' => $categoryList, 'productCategories' => $selected]);
    }

    public function save($id, Request $request)
    {
        // dd($id);
        $msg = [
            "categories.required" => 'Please Choose categories!',
            "image.required" => 'Please Select Images!',
            "v_name.required" => 'Please enter product name!',
            "f_price.required" => 'Please enter price!',
            "f_price.numeric" => 'Price Must be a number!',
            "f_sale_price.required" => 'Please enter sale-price!',
            "f_sale_price.numeric" => 'Sale-price Must be a number!',
            "i_qty.required" => 'Please enter quantity!',
            "i_qty.numeric" => 'Quantity Must be a number!',
            "i_order.required" => 'Please enter orders!',
            "i_order.numeric" => 'Orders Must be a number!',
            "ti_status.required" => 'Please Select Status!'
        ];

        $validator = validator::make(
            $request->all(),
            [
                'categories' => 'required',
                'v_name' => 'required',
                'f_price' => 'required|numeric',
                'f_sale_price' => 'required|numeric',
                'i_qty' => 'required|numeric',
                'i_order' => 'required|numeric',
                'ti_status' => 'required'
            ],
            $msg
        );

        if (!$id) {
            $validator = validator::make($request->all(), [
                'categories' => 'required',
                'image'=> 'required',
                'v_name' => 'required',
                'f_price' => 'required|numeric',
                'f_sale_price' => 'required|numeric',
                'i_qty' => 'required|numeric',
                'i_order' => 'required|numeric',
                'ti_status' => 'required'
            ], $msg);
        }
        if ($validator->fails()) {
            return response()->json(['err' => $validator->errors()->all()[0]]);
        }
        $product = Product::find($id);
        $product = $product ? $product : new Product;

        $product->v_name = $request->v_name;
        $product->i_product_code = mt_rand(1, 100000);
        $product->f_price = $request->f_price;
        $product->f_sale_price = $request->f_sale_price;
        $product->i_qty = $request->i_qty;
        $product->i_order = $request->i_order;
        $product->ti_status = $request->ti_status;

        $categoryList = $request->categories;

        if ($id) {
            $imageold = ProductImage::where('i_product_id', $request->id)->update(['ti_main_image' => '0']);
            $imgnew =  ProductImage::where('id', $request->input('selectedMainImage'))->update(['ti_main_image' => '1']);
        }

        $imageData = ((isset($request->image) && !empty($request->image))) ? $request->image : [];
        // dd($request->image);
        if ($product->save()) {
            // dd($product->id);
            $status=$this->imageSave($product->id, $imageData);
            $this->categoryList($product->id, $categoryList);
            if(empty($status)){
                $status=['success' => 'Product Add Success', 'url' => route('product-index')];
            }
            return response()->json($status);
        }
    }

    public function imageSave($productId, $imageData)

    {
        // dd($productId);
        if (!empty($imageData)) {
            $imageCount = ProductImage::where('i_product_id', $productId)->count();
            if ($imageCount == 0) {
                $imageUpload = ImageInsert($imageData);
                foreach ($imageUpload as $key => $val) {
                    if ($key != 'err') {
                        $image[] = [
                            'v_image' => $val,
                            'i_product_id' => $productId,
                            'ti_main_image' => $key == 0 ?   1 :  0
                        ];
                    } else { 
                        return response()->json($imageUpload);
                    }
                }
                ProductImage::insert($image);
            } else {
                $imageUpload = ImageInsert($imageData);
                foreach ($imageUpload as $key => $val) {
                    if ($key != 'err') {
                        $image[] = [
                            'v_image' => $val,
                            'i_product_id' => $productId,
                            'ti_main_image' => 0
                        ];
                    } else {
                        // dd($imageUpload);
                       return $imageUpload;
                    }
                }
                ProductImage::insert($image);
            }
        }
    }

    public function categoryList($productId, $categoryList)
    {
        if ($productId) {
            $productCat = ProductCategories::where('bi_product_id', '=', $productId)->delete();
        }
        foreach ($categoryList as $key => $val) {
            $category[$key] = [
                'bi_product_id' => $productId,
                'bi_category_id' => $val
            ];
        }
        ProductCategories::insert($category);
    }

    public function delete(Request $request)
    {
        $product = Product::find($request->id);
        $product->delete();
        return response()->json(['success' => 'Deleted']);
    }

    public function imageDelete(Request $request)
    {
        $imageDelete = ProductImage::find($request->id);
        $imageDelete->delete();
        return response()->json(['success' => 'Deleted']);
    }
}
