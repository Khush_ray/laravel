<?php

namespace App\Http\Controllers;

use App\Models\SiteSettings;

use Illuminate\Http\Request;

use function PHPUnit\Framework\isEmpty;

class SiteSettingsController extends Controller
{
    public function index()
    {
        $data = [];
        $title = 'Site-Settings';
        $site_settings = SiteSettings::all();
        // dd($site_settings);
        foreach ($site_settings as $key => $val) {
            $data[] = $val->l_val;
        }
        // dd($data);
        return view('site-settings.SiteSettings', ['title' => $title, 'data' => $data]);
    }

    public function save(Request $request)
    {
        // dd($request);
        if($request->hasFile('site_logo')){
            $ext = ['jpg', 'jpeg', 'png', 'gif'];
            $image_ext = $request->site_logo->extension();
            if ($request->site_logo) {
                if (in_array($image_ext, $ext)) {
                    $image_name = time() . '_' . rand(100, 10000) . '.' . $image_ext;
                    $request->site_logo->move(public_path('image/siteSettings'), $image_name);
                } else {
                    return response()->json(['err' => "Image must be in jpg/jpeg/png/gif format"]);
                }
            }
            $data = [
                ['v_key' => 'site_name', 'l_val' => request('site_title')],
                ['v_key' => 'site_logo', 'l_val' => $image_name]
            ];
            // dd($image_name);
            $site_data = SiteSettings::all();
            // dd($site_data);

            if (empty($site_data)) {
                SiteSettings::insert($data);
                return redirect('site-settings');
            }
            if ($site_data[0]->v_key == 'site_name') {
                // dd('Name changes while image ');
                $site_title = SiteSettings::where(['v_key' => 'site_name'])->update(['l_val' => request('site_title')]);
            }
            if ($site_data[1]->v_key == 'site_logo') {
                // dd('image change while image');
                $site_logo = SiteSettings::where(['v_key' => 'site_logo'])->update(['l_val' => $image_name]);
            }
         }
         else{
            $site_data = SiteSettings::all();
            if ($site_data[0]->v_key == 'site_name') {
                $site_title = SiteSettings::where(['v_key' => 'site_name'])->update(['l_val' => request('site_title')]);
                // dd('hi2');
            }
         }
        return redirect('site-settings');
    }
}
