<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SiteSettings extends Model
{
    public $table = 'tbl_site_settings';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $fillable = ['v_key', 'l_val'];
}
