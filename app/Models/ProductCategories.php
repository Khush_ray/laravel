<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductCategories extends Model
{
    public $table = 'tbl_product_categories';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $fillable = ['bi_product_id','bi_category_id'];}
