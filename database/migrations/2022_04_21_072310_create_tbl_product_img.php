<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblProductImg extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_product_img', function (Blueprint $table) {
            $table->id();
            $table->integer('i_product_id');
            $table->string('v_image',255);
            $table->tinyInteger('ti_main_image')->comment('1=active , 0=inactive');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_product_img');
    }
}
