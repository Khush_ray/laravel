<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateTblProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_product', function (Blueprint $table) {
            $table->id();
            $table->string('v_name',255);
            $table->string('v_categories',255);
            $table->integer('i_product_code');
            $table->float('f_price');
            $table->float('f_sale_price');
            $table->integer('i_qty');
            $table->timestamp('dt_added_on')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('dt_modified_on')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->integer('i_order');
            $table->tinyInteger('ti_status')->comment('1=active , 0=inactive');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_product');
    }
}
